package com.watergate.watergate;

/**
 * Created by Kiragames on 3/11/2017 AD.
 */

class WaterGateActivityJSON {
    public String ph;
    public String temp;
    public String topWater;
    public String bottomWater;
    public String flowWater;
    public String depthTop;
    public String depthBottom;
    public String volume;


    public String[] flowFragment;
    public String[] openFragment;
    public String[] currentFragment;

    WaterGateActivityJSON() {
        ph = "";
        temp = "";
        topWater = "";
        bottomWater = "";
        flowWater = "";
        flowFragment = new String[6];
        openFragment = new String[6];
        currentFragment = new String[6];
    }
}

