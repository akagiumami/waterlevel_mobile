package com.watergate.watergate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

/**
 * Created by Bryanz on 6/19/2017 AD.
 */

public class DisplaysActivity extends AppCompatActivity {

    public static final String TAG = "DisplaysActivity";

    private boolean isShowTopWater = true;
    private boolean isShowBottomWater = true;
    private boolean isShowFlow = true;
    private boolean isShowVolume = true;
    private boolean isShowTopDepth = true;
    private boolean isShowBottomDepth = true;
    private boolean isShowPh = true;
    private boolean isShowTemp = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displays_activity);
        getSupportActionBar().setTitle("Displays Setting");

        Log.d(TAG, "getPreferenceData()");
        getPreferenceData();
        setDisplayData();

        Button resetButton = (Button) findViewById(R.id.displaysResetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShowTopWater = true;
                isShowBottomWater = true;
                isShowFlow = true;
                isShowVolume = true;
                isShowTopDepth = true;
                isShowBottomDepth = true;
                isShowPh = true;
                isShowTemp = true;
                setDisplayData();
                Toast.makeText(DisplaysActivity.this, "กลับไปสู่ค่าเริ่มต้น", Toast.LENGTH_SHORT).show();

            }
        });

        Button submitButton = (Button) findViewById(R.id.displaysSubmitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {;
                bindData();
                Toast.makeText(DisplaysActivity.this, "บันทึกข้อมูลเรียบร้อยแล้ว", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPreferenceData() {

        Context context = DisplaysActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);

        isShowTopWater = sharedPref.getBoolean(getString(R.string.is_topwater_key), true);
        isShowBottomWater = sharedPref.getBoolean(getString(R.string.is_bottomwater_key), true);
        isShowFlow = sharedPref.getBoolean(getString(R.string.is_flow_key), true);
        isShowVolume = sharedPref.getBoolean(getString(R.string.is_volume_key), true);
        isShowTopDepth = sharedPref.getBoolean(getString(R.string.is_topdepth_key), true);
        isShowBottomDepth = sharedPref.getBoolean(getString(R.string.is_bottomdepth_key), true);
        isShowPh = sharedPref.getBoolean(getString(R.string.is_ph_key), true);
        isShowTemp = sharedPref.getBoolean(getString(R.string.is_temp_key), true);
    }

    private void setDisplayData() {
        CheckBox phShowBox = (CheckBox) findViewById(R.id.phShowBox);
        phShowBox.setChecked(isShowPh);

        CheckBox tempShowBox = (CheckBox) findViewById(R.id.tempShowBox);
        tempShowBox.setChecked(isShowTemp);

        CheckBox topWaterShowBox = (CheckBox) findViewById(R.id.topWaterShowBox);
        topWaterShowBox.setChecked(isShowTopWater);

        CheckBox bottomWaterShowBox = (CheckBox) findViewById(R.id.bottomWaterShowBox);
        bottomWaterShowBox.setChecked(isShowBottomWater);

        CheckBox depthTopShowBox = (CheckBox) findViewById(R.id.depthTopShowBox);
        depthTopShowBox.setChecked(isShowTopDepth);

        CheckBox depthBottomShowBox = (CheckBox) findViewById(R.id.depthBottomShowBox);
        depthBottomShowBox.setChecked(isShowBottomDepth);

        CheckBox flowShowBox = (CheckBox) findViewById(R.id.flowShowBox);
        flowShowBox.setChecked(isShowFlow);

        CheckBox volumeShowBox = (CheckBox) findViewById(R.id.volumeShowBox);
        volumeShowBox.setChecked(isShowVolume);

    }

    private void bindData() {

        CheckBox phShowBox = (CheckBox) findViewById(R.id.phShowBox);
        isShowPh = phShowBox.isChecked();

        CheckBox tempShowBox = (CheckBox) findViewById(R.id.tempShowBox);
        isShowTemp = tempShowBox.isChecked();

        CheckBox topWaterShowBox = (CheckBox) findViewById(R.id.topWaterShowBox);
        isShowTopWater = topWaterShowBox.isChecked();

        CheckBox bottomWaterShowBox = (CheckBox) findViewById(R.id.bottomWaterShowBox);
        isShowBottomWater = bottomWaterShowBox.isChecked();

        CheckBox depthTopShowBox = (CheckBox) findViewById(R.id.depthTopShowBox);
        isShowTopDepth = depthTopShowBox.isChecked();

        CheckBox depthBottomShowBox = (CheckBox) findViewById(R.id.depthBottomShowBox);
        isShowBottomDepth = depthBottomShowBox.isChecked();

        CheckBox flowShowBox = (CheckBox) findViewById(R.id.flowShowBox);
        isShowFlow = flowShowBox.isChecked();

        CheckBox volumeShowBox = (CheckBox) findViewById(R.id.volumeShowBox);
        isShowVolume = volumeShowBox.isChecked();

        Context context = DisplaysActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.is_topwater_key), isShowTopWater);
        editor.putBoolean(getString(R.string.is_bottomwater_key), isShowBottomWater);
        editor.putBoolean(getString(R.string.is_flow_key), isShowFlow);
        editor.putBoolean(getString(R.string.is_volume_key), isShowVolume);
        editor.putBoolean(getString(R.string.is_topdepth_key), isShowTopDepth);
        editor.putBoolean(getString(R.string.is_bottomdepth_key), isShowBottomDepth);
        editor.putBoolean(getString(R.string.is_ph_key), isShowPh);
        editor.putBoolean(getString(R.string.is_temp_key), isShowTemp);
        editor.commit();
    }
}
