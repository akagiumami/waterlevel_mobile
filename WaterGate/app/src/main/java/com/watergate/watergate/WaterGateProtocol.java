package com.watergate.watergate;

import android.app.ExpandableListActivity;
import android.content.SharedPreferences;
import android.content.Context;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Bryanz on 5/13/2017 AD.
 */

public class WaterGateProtocol extends AsyncTask<Void, Integer, String>{

    public static final String TAG = "WaterGateProtocol";
    private final String USER_AGENT = "Mozilla/5.0";
    private Context mContext;
    private String mIpAddress;
    private String mApiAddress;

    public WaterGateProtocol(AsyncResponse delegate, Context context, String url, String api) {
        mContext = context;
        mIpAddress = url;
        this.delegate = delegate;
        mApiAddress = api;

        SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        mIpAddress = sharedPref.getString(mContext.getString(R.string.ip_key), "127.0.0.1");
//        Log.d(TAG, "Ip: " + mIpAddress);

    }

    // you may separate this or combined to caller class.
    public interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    @Override
    protected void onProgressUpdate(Integer... values) {

    }

    @Override
    protected String doInBackground(Void... params) {
        String result = "";
        try {
//            Log.d(TAG, "loadDataFromIP by interval");

            SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preference_file_key), mContext.MODE_PRIVATE);
            mIpAddress = sharedPref.getString(mContext.getString(R.string.ip_key), "127.0.0.1");
//            Log.d(TAG, "Ip: " + mIpAddress);
            String jsonString = new String();

            if (mIpAddress.equals("test")) {
                jsonString = "{\"g1\":{\"flow\":368.3489990234375,\"current\":15,\"position\":4.027633190155029},\"g2\":{\"flow\":368.3489990234375,\"position\":4,\"current\":15},\"g3\":{\"flow\":368.3489990234375,\"position\":4,\"current\":15.034722328186035},\"g4\":{\"flow\":368.3489990234375,\"position\":4,\"current\":15},\"g5\":{\"flow\":368.3489990234375,\"position\":4,\"current\":15},\"g6\":{\"flow\":368.3489990234375,\"position\":4,\"current\":15},\"tail\":302.7998352050781,\"ph\":5.62480354309082,\"temp\":30.94710350036621,\"head\":301.5778503417969,\"flow\":368.3489990234375,\"water_quantity\":100.000}";
            } else {
                HttpURLConnection urlConnection = null;

                URL url = new URL(mIpAddress + mApiAddress);

                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000 /* milliseconds */);
                urlConnection.setConnectTimeout(10000 /* milliseconds */);

                urlConnection.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

                char[] buffer = new char[1024];

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                jsonString = sb.toString();

            }
            result = jsonString;
//            Log.d(TAG, "result" + result);
        }
        catch (Exception e) {
            Log.d(TAG, "Exception:" + e.toString());
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }

}
