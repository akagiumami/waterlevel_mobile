package com.watergate.watergate;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class WaterGateActivity extends AppCompatActivity {
    public static final String TAG = "WaterGateActivity";
    private int mIntervalValue = 1;
    private String mIpAddress = "http://jotarokaga.ddns.net:3000";
    private String mSMSWords = "ข้อความ";
    private Runnable mRefreshRunnable;
    private int alarmCounter;
    private WaterGateActivityJSON mJSONObject = new WaterGateActivityJSON();
    public static WaterGateProtocol protocol;
    private boolean isFirstRun = true;
    private boolean isShowTopWater = true;
    private boolean isShowBottomWater = true;
    private boolean isShowFlow = true;
    private boolean isShowVolume = true;
    private boolean isShowTopDepth = true;
    private boolean isShowBottomDepth = true;
    private boolean isShowPh = true;
    private boolean isShowTemp = true;

    private final Handler handler = new Handler();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Log.d(TAG, "onBackPressed");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.watergate_activity);
        alarmCounter = 0;
        initPreferenceData();
        isFirstRun = true;
        showHideDisplay();
        protocol = new WaterGateProtocol(new WaterGateProtocol.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                if (!output.isEmpty()) {
//                    Log.d(TAG, "output:" + output);
                    refreshDisplay(output);
                }
            }
        }, this, mIpAddress, "/getCurrInfo");
        protocol.execute();

        mJSONObject = new WaterGateActivityJSON();

        ImageButton refreshButton = (ImageButton) findViewById(R.id.refreshButton);

        Log.d(TAG, "On Create");

        mRefreshRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    newAsycInstance();
                }
                catch (Exception e) {
//                    Log.d(TAG, "Protocol Error");
                    Log.d(TAG, "Exception:" + e.toString());
                }
                doAutoRefresh();
                sendNotification();

            }
        };
        doAutoRefresh();


        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    newAsycInstance();
                }
                catch (Exception e) {
                    Log.d(TAG, "Exception:" + e.toString());
                }
                Toast.makeText(WaterGateActivity.this, "รีเฟรชเรียบร้อยแล้ว", Toast.LENGTH_SHORT).show();

            }
        });

        ImageButton settingButton = (ImageButton) findViewById(R.id.settingButton);

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(view);
            }
        });

    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_settings) {
                    Intent myIntent = new Intent(WaterGateActivity.this, SettingsActivity.class);
                    WaterGateActivity.this.startActivity(myIntent);
                    handler.removeCallbacks(mRefreshRunnable);
                    return true;
                }
                if (id == R.id.action_daily) {
                    Intent myIntent = new Intent(WaterGateActivity.this, DailyActivity.class);
                    WaterGateActivity.this.startActivity(myIntent);
                    handler.removeCallbacks(mRefreshRunnable);
                    return true;
                }
                if (id == R.id.action_interval) {

                    return true;
                }
                if (id == R.id.action_monthly) {

                    return true;
                }

                return false;
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_water_gate, popup.getMenu());
        popup.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        Log.d(TAG, "onCreateOptionsMenu");
//        getMenuInflater().inflate(R.menu.menu_water_gate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // as you specify a parent activity in AndroidManifest.xml.
//        Log.d(TAG, "onOptionsItemSelected");
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void loadDisplayDataFromJSON(String output) {
//        Log.d(TAG, "loadDisplayDataFromJSON");
        try {
            //String jsonString = "{\"g1\":{\"flow\":368.3489990234375,\"current\":15,\"position\":4.027633190155029},\"g2\":{\"position\":4,\"current\":15},\"g3\":{\"position\":4,\"current\":15.034722328186035},\"g4\":{\"position\":4,\"current\":15},\"g5\":{\"position\":4,\"current\":15},\"g6\":{\"position\":4,\"current\":15},\"tail\":302.7998352050781,\"ph\":5.62480354309082,\"temp\":30.94710350036621,\"head\":301.5778503417969,\"flow\":368.3489990234375}";
            String jsonString = output;
            if (jsonString != null && mJSONObject != null) {
                JSONObject jsonObject = new JSONObject(jsonString);

                if (jsonObject.has("ph")) {
                    String ph = jsonObject.getString("ph");
                    if (!ph.isEmpty())
                        mJSONObject.ph = String.format("%.3f", Float.parseFloat(ph));
                }
                if (jsonObject.has("temp")) {
                    String temp = jsonObject.getString("temp");
                    if (!temp.isEmpty())
                        mJSONObject.temp = String.format("%.3f", Float.parseFloat(temp));
                }

                if (jsonObject.has("head")) {
                    String head = jsonObject.getString("head");
                    if (!head.isEmpty()) {
                        mJSONObject.topWater = String.format("%.3f", Float.parseFloat(head));
                        mJSONObject.depthTop = String.format("%.3f", Float.valueOf(mJSONObject.topWater) - 295);
                    }
                }

                if (jsonObject.has("tail")) {
                    String tail = jsonObject.getString("tail");
                    if (!tail.isEmpty()) {
                        mJSONObject.bottomWater = String.format("%.3f", Float.parseFloat(tail));
                        mJSONObject.depthBottom = String.format("%.3f", Float.valueOf(mJSONObject.bottomWater) - 295);
                    }
                }

                if (jsonObject.has("flow")) {
                    String flow = jsonObject.getString("flow");
                    if (!flow.isEmpty())
//                        Log.d(TAG, "flow:" + flow);
                        mJSONObject.flowWater = String.format("%.3f", Float.parseFloat(flow));
                }

                if (jsonObject.has("water_quantity")) {
                    String volume = jsonObject.getString("water_quantity");
                    if (!volume.isEmpty())
//                        Log.d(TAG, "volume:" + volume);
                        mJSONObject.volume = String.format("%.3f", Float.parseFloat(volume));
                }
            }

        }
        catch (JSONException ex) {
//            Toast.makeText(WaterGateActivity.this, "JSON Parse Error", Toast.LENGTH_SHORT).show();
        }

    }

    private void loadFragmentDataFromJSONByGate(int gate, String jsonString) {
//        Log.d(TAG, "loadFragmentDataFromJSON Gate: " + mGate);
        try {
            String fragmentString = "";
            try {

                JSONObject jsonObject = new JSONObject(jsonString);
                JSONObject waterGate = new JSONObject();
                switch (gate) {
                    case 0:
                        break;
                    case 1:
                        waterGate = jsonObject.getJSONObject("g1");
                        break;
                    case 2:
                        waterGate = jsonObject.getJSONObject("g2");
                        break;
                    case 3:
                        waterGate = jsonObject.getJSONObject("g3");
                        break;
                    case 4:
                        waterGate = jsonObject.getJSONObject("g4");
                        break;
                    case 5:
                        waterGate = jsonObject.getJSONObject("g5");
                        break;
                    case 6:
                        waterGate = jsonObject.getJSONObject("g6");
                        break;
                    default:
                        break;
                }

                fragmentString = waterGate.toString();
            }
            catch (Exception e) {
//                Log.d(TAG, "JSON Fragment Error");
            }

            if (jsonString != null && mJSONObject != null) {
                JSONObject jsonObject = new JSONObject(fragmentString);
                if (jsonObject.has("flow")) {
                    String flow = jsonObject.getString("flow");
                    if (!flow.isEmpty())
                        mJSONObject.flowFragment[gate-1] = String.format("%.3f", Float.parseFloat(flow));
                }
                else {
                    mJSONObject.flowFragment[gate-1] = "0";
                }
                if (jsonObject.has("position")) {
                    String position = jsonObject.getString("position");
                    if (!position.isEmpty())
                        mJSONObject.openFragment[gate - 1] = String.format("%.3f", Float.parseFloat(position));
                }
                else {
                    mJSONObject.openFragment[gate-1] = "0";
                }
                if (jsonObject.has("current")) {
                    String current = jsonObject.getString("current");
                    if (!current.isEmpty())
                        mJSONObject.currentFragment[gate-1] = String.format("%.3f", Float.parseFloat(current));
                }
                else {
                    mJSONObject.currentFragment[gate-1] = "0";
                }
            }
        }
        catch (JSONException ex) {
//            Toast.makeText(getActivity(), "JSON Fragment Parse Error", Toast.LENGTH_SHORT).show();
        }

    }


    private void refreshDisplay(String output) {
//        Log.d(TAG, "refreshDisplay/ currentInterval:" + mIntervalValue);

        //GetData from JSON
        loadDisplayDataFromJSON(output);

        //Refresh Display View
        TextView phTextView = (TextView) findViewById(R.id.phTextView);
        phTextView.setText(mJSONObject.ph);
        TextView tempTextView = (TextView) findViewById(R.id.tempTextView);
        tempTextView.setText(mJSONObject.temp + " ˚C");
        TextView topWaterTextView = (TextView) findViewById(R.id.topWaterTextView);
        topWaterTextView.setText(mJSONObject.topWater + " ม.รทก");
        TextView bottomTextView = (TextView) findViewById(R.id.bottomWaterTextView);
        bottomTextView.setText(mJSONObject.bottomWater + " ม.รทก");
        TextView flowTextView = (TextView) findViewById(R.id.flowWaterTextView);
        flowTextView.setText(mJSONObject.flowWater + " m³/s");
        TextView volumeTextView = (TextView) findViewById(R.id.volumeTextView);
        volumeTextView.setText(mJSONObject.volume + " ล.ลบ.ม.");
        TextView depthTopTextView = (TextView) findViewById(R.id.depthTopTextView);
        depthTopTextView.setText(mJSONObject.depthTop + " ม.รทก");
        TextView depthBottomTextView = (TextView) findViewById(R.id.depthBottomTextView);
        depthBottomTextView.setText(mJSONObject.depthBottom + " ม.รทก");

        for (int i = 1; i <= 6; i++) {
            loadFragmentDataFromJSONByGate(i, output);

            switch (i) {
                case 0:
                    break;
                case 1:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate1Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate1Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate1Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                case 2:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate2Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate2Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate2Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                case 3:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate3Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate3Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate3Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                case 4:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate4Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate4Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate4Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                case 5:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate5Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate5Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate5Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                case 6:
                    if (mJSONObject.flowFragment[i-1] != null) {
                        TextView flowFragmentTextView = (TextView) findViewById(R.id.gate6Flow);
                        flowFragmentTextView.setText(mJSONObject.flowFragment[i-1] + "\nm³/s");
                    }
                    if (mJSONObject.openFragment[i-1] != null) {
                        TextView openFragmentTextView = (TextView) findViewById(R.id.gate6Open);
                        openFragmentTextView.setText(mJSONObject.openFragment[i-1] + " m.");
                    }

                    if (mJSONObject.currentFragment[i-1] != null) {
                        TextView currentFragmentTextView = (TextView) findViewById(R.id.gate6Current);
                        currentFragmentTextView.setText(mJSONObject.currentFragment[i-1] + " A.");
                    }
                    break;
                default:
                    break;
            }
        }

    }

    private void initPreferenceData() {

        Log.d(TAG, "initPreferenceData");
        Context context = WaterGateActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);

        mIpAddress = sharedPref.getString(getString(R.string.ip_key), "http://jotarokaga.ddns.net:3000/getCurrInfo");
        mSMSWords = sharedPref.getString(getString(R.string.sms_key), "ข้อความ");
        mIntervalValue = sharedPref.getInt(getString(R.string.interval_key), 1);

        isShowTopWater = sharedPref.getBoolean(getString(R.string.is_topwater_key), true);
        isShowBottomWater = sharedPref.getBoolean(getString(R.string.is_bottomwater_key), true);
        isShowFlow = sharedPref.getBoolean(getString(R.string.is_flow_key), true);
        isShowVolume = sharedPref.getBoolean(getString(R.string.is_volume_key), true);
        isShowTopDepth = sharedPref.getBoolean(getString(R.string.is_topdepth_key), true);
        isShowBottomDepth = sharedPref.getBoolean(getString(R.string.is_bottomdepth_key), true);
        isShowPh = sharedPref.getBoolean(getString(R.string.is_ph_key), true);
        isShowTemp = sharedPref.getBoolean(getString(R.string.is_temp_key), true);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.ip_key), mIpAddress);
        editor.putString(getString(R.string.sms_key), mSMSWords);
        editor.putInt(getString(R.string.interval_key), mIntervalValue);
        editor.putBoolean(getString(R.string.is_topwater_key), isShowTopWater);
        editor.putBoolean(getString(R.string.is_bottomwater_key), isShowBottomWater);
        editor.putBoolean(getString(R.string.is_flow_key), isShowFlow);
        editor.putBoolean(getString(R.string.is_volume_key), isShowVolume);
        editor.putBoolean(getString(R.string.is_topdepth_key), isShowTopDepth);
        editor.putBoolean(getString(R.string.is_bottomdepth_key), isShowBottomDepth);
        editor.putBoolean(getString(R.string.is_ph_key), isShowPh);
        editor.putBoolean(getString(R.string.is_temp_key), isShowTemp);
        editor.commit();

    }

    private void doAutoRefresh() {
//        Log.d(TAG, "bind doAutoRefresh");
        handler.removeCallbacks(mRefreshRunnable);
        if (isFirstRun) {
            handler.postDelayed(mRefreshRunnable, 1 * 1000);
            isFirstRun = false;
        } else {
            handler.postDelayed(mRefreshRunnable, mIntervalValue * 1000);
        }
    }

    private void newAsycInstance() {

        protocol = new WaterGateProtocol(new WaterGateProtocol.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                if (!output.isEmpty()) {
//                    Log.d(TAG, "output:" + output);
                    refreshDisplay(output);
                }
            }
        }, this, mIpAddress, "/getCurrInfo");
        protocol.execute();
    }

    private void sendNotification () {

        alarmCounter += mIntervalValue;
        Context context = WaterGateActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);
        int alarmValue =  sharedPref.getInt(getString(R.string.alarm_key), 0);
        float veryHigh = sharedPref.getFloat(getString(R.string.veryhigh_key), 0);
        float high = sharedPref.getFloat(getString(R.string.high_key), 0);
        float low = sharedPref.getFloat(getString(R.string.low_key), 0);
        float veryLow = sharedPref.getFloat(getString(R.string.verylow_key), 0);
        float flow = sharedPref.getFloat(getString(R.string.flow_key), 0);
        String notifyMessage = "";

//        Log.d(TAG, "AlarmCounter:" + alarmCounter + "AlarmValue:" + ((alarmValue+1)*5)*60);
        boolean isNotify = false;


        if (!mJSONObject.topWater.isEmpty()) {
            float temp = Float.parseFloat(mJSONObject.topWater);
            float topWater = Float.parseFloat(String.format("%.1f", temp));
//            Log.d(TAG, "topWater:" + topWater +  " == " + "veryHigh: " + veryHigh);
            if (topWater == veryHigh) {
                isNotify = true;
                notifyMessage += "ระดับน้ำสูงมาก\n";
            }
            else if (topWater == high) {
                isNotify = true;
                notifyMessage += "ระดับน้ำสูง\n";
            }
            else if (topWater == low) {
                isNotify = true;
                notifyMessage += "ระดับน้ำสูงต่ำ\n";
            }
            else if (topWater == veryLow) {
                isNotify = true;
                notifyMessage += "ระดับน้ำสูงต่ำมาก\n";
            }
            else {
            }
            notifyMessage += topWater;

        }

        if (!mJSONObject.flowWater.isEmpty()) {
            float flowGauge = Float.parseFloat(mJSONObject.flowWater);
//            Log.d(TAG, "flowGauge:" + flowGauge +  " > " + "flow: " + flow);
            if (flowGauge > flow) {
                isNotify = true;
                notifyMessage += "อัตราการไหลมากกว่าค่าที่กำหนด";
                notifyMessage += flowGauge;
            }
        }
        if (alarmCounter > ((alarmValue+1))*60*5 && isNotify) {
            PendingIntent contentIntent = PendingIntent.getActivity(WaterGateActivity.this, 0,
                    new Intent(WaterGateActivity.this, AlarmListener.class), 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(WaterGateActivity.this);
            mBuilder.setSmallIcon(R.drawable.alerticon);
            mBuilder.setContentTitle("WaterGate Alert").setContentText(notifyMessage);
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setDefaults(Notification.DEFAULT_SOUND);
            mBuilder.setAutoCancel(true);
            NotificationManager mNotificationManager = (NotificationManager) WaterGateActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(1, mBuilder.build());
            alarmCounter = 0;
        }

    }

    private void showHideDisplay() {

        TextView phTextView = (TextView) findViewById(R.id.phTextView);
        phTextView.setVisibility(isShowPh == true ? View.VISIBLE : View.GONE);
        TextView phTextLabel = (TextView) findViewById(R.id.phLabel);
        phTextLabel.setVisibility(isShowPh == true ? View.VISIBLE : View.GONE);

        TextView tempTextView = (TextView) findViewById(R.id.tempTextView);
        tempTextView.setVisibility(isShowTemp == true ? View.VISIBLE : View.GONE);
        TextView tempTextLabel = (TextView) findViewById(R.id.tempLabel);
        tempTextLabel.setVisibility(isShowTemp == true ? View.VISIBLE : View.GONE);

        TextView topWaterTextView = (TextView) findViewById(R.id.topWaterTextView);
        topWaterTextView.setVisibility(isShowTopWater == true ? View.VISIBLE : View.GONE);
        TextView topWaterTextLabel = (TextView) findViewById(R.id.topWaterLabel);
        topWaterTextLabel.setVisibility(isShowTopWater == true ? View.VISIBLE : View.GONE);

        TextView bottomTextView = (TextView) findViewById(R.id.bottomWaterTextView);
        bottomTextView.setVisibility(isShowBottomWater == true ? View.VISIBLE : View.GONE);
        TextView bottomTextLabel = (TextView) findViewById(R.id.bottomWaterLabel);
        bottomTextLabel.setVisibility(isShowBottomWater == true ? View.VISIBLE : View.GONE);

        TextView flowTextView = (TextView) findViewById(R.id.flowWaterTextView);
        flowTextView.setVisibility(isShowFlow == true ? View.VISIBLE : View.GONE);
        TextView flowTextLabel = (TextView) findViewById(R.id.flowWaterLabel);
        flowTextLabel.setVisibility(isShowFlow == true ? View.VISIBLE : View.GONE);

        TextView volumeTextView = (TextView) findViewById(R.id.volumeTextView);
        volumeTextView.setVisibility(isShowVolume == true ? View.VISIBLE : View.GONE);
        TextView volumeTextLabel = (TextView) findViewById(R.id.volumeLabel);
        volumeTextLabel.setVisibility(isShowVolume == true ? View.VISIBLE : View.GONE);

        TextView depthTopTextView = (TextView) findViewById(R.id.depthTopTextView);
        depthTopTextView.setVisibility(isShowTopDepth == true ? View.VISIBLE : View.GONE);
        TextView depthTopTextLabel = (TextView) findViewById(R.id.depthTopLabel);
        depthTopTextLabel.setVisibility(isShowTopDepth == true ? View.VISIBLE : View.GONE);

        TextView depthBottomTextView = (TextView) findViewById(R.id.depthBottomTextView);
        depthBottomTextView.setVisibility(isShowBottomDepth == true ? View.VISIBLE : View.GONE);
        TextView depthBottomTextLabel = (TextView) findViewById(R.id.depthBottomLabel);
        depthBottomTextLabel.setVisibility(isShowBottomDepth == true ? View.VISIBLE : View.GONE);
    }
}
