package com.watergate.watergate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    private Spinner mIntervalSpinner;
    private int mIntervalValue = 1;
    private String mIpAddress = "http://jotarokaga.ddns.net:3000/getCurrInfo";
    private String mSMSWords = "ข้อความ";
    private int mAlarmValue = 1;
    private float mVeryHigh = 0;
    private float mHigh = 0;
    private float mLow = 0;
    private float mVeryLow = 0;
    private float mFlow = 0;
    public static final String TAG = "SettingsActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportActionBar().setTitle(getResources().getString(R.string.action_settings));

        mIntervalSpinner = (Spinner) findViewById(R.id.intervalSpinner);
//        mAlarmSpinner = (Spinner) findViewById(R.id.alarmSpinner);
        createSpinnerList(12);

        Log.d(TAG, "getPreferenceData()");
        getPreferenceData();
        setDisplayData();

        Button resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIpAddress = "http://jotarokaga.ddns.net:3000/getCurrInfo";
                mSMSWords = "ข้อความ";
                mIntervalValue = 1;
                mAlarmValue = 0;
                mVeryHigh = 0;
                mHigh = 0;
                mLow = 0;
                mVeryLow = 0;
                mFlow = 0;

                setDisplayData();
                Toast.makeText(SettingsActivity.this, "กลับไปสู่ค่าเริ่มต้น", Toast.LENGTH_SHORT).show();

            }
        });

        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {;
                bindData();
                Toast.makeText(SettingsActivity.this, "บันทึกข้อมูลเรียบร้อยแล้ว", Toast.LENGTH_SHORT).show();
            }
        });

        Button displaysButton = (Button) findViewById(R.id.displaysButton);
        displaysButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(SettingsActivity.this, DisplaysActivity.class);
                SettingsActivity.this.startActivity(myIntent);
            }
        });
    }

    private void createSpinnerList(int maxValue) {
        List<Integer> valueList = new ArrayList<Integer>();
        List<Integer> alarmList = new ArrayList<Integer>();
        for (int i = 1; i <= maxValue; i++ ) {
            valueList.add(i);
            alarmList.add(i*5);
        }
        if (mIntervalSpinner != null) {
            ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, valueList);
            mIntervalSpinner.setAdapter(adapter);
        }
//        if (mAlarmSpinner != null) {
//            ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, alarmList);
//            mAlarmSpinner.setAdapter(adapter2);
//        }

    }

    private void bindData() {
        EditText ipTextBox = (EditText) findViewById(R.id.ipTextBox);
        mIpAddress = ipTextBox.getText().toString();
        EditText smsTextBox = (EditText) findViewById(R.id.smsTextBox);
        mSMSWords = smsTextBox.getText().toString();
        mIntervalValue = (int)mIntervalSpinner.getSelectedItem();
//        mAlarmValue = mAlarmSpinner.getSelectedItemPosition();
        EditText alarmTextBox = (EditText) findViewById(R.id.alarmTextBox);
        mAlarmValue = Integer.parseInt(alarmTextBox.getText().toString());

        EditText veryHighText = (EditText) findViewById(R.id.veryHighTextBox);
        if (veryHighText.getText().length() > 0 ) {
            float temp = Float.parseFloat(veryHighText.getText().toString());
            mVeryHigh = Float.parseFloat(String.format("%.1f", temp));
        } else {
            mVeryHigh = 0.0f;
        }

        EditText highText = (EditText) findViewById(R.id.highTextBox);
        if (highText.getText().length() > 0 ) {
            float temp = Float.parseFloat(highText.getText().toString());
            mHigh = Float.parseFloat(String.format("%.1f", temp));
        } else {
            mHigh = 0.0f;
        }

        EditText lowText = (EditText) findViewById(R.id.lowTextBox);
        if (lowText.getText().length() > 0 ) {
            float temp = Float.parseFloat(lowText.getText().toString());
            mLow = Float.parseFloat(String.format("%.1f", temp));
        } else {
            mLow = 0.0f;
        }
        EditText veryLowText = (EditText) findViewById(R.id.veryLowTextBox);
        if (veryLowText.getText().length() > 0 ) {
            float temp = Float.parseFloat(veryLowText.getText().toString());
            mVeryLow = Float.parseFloat(String.format("%.1f", temp));
        } else {
            mVeryLow = 0.0f;
        }

        EditText flowText = (EditText) findViewById(R.id.flowSettingTextBox);
        if (flowText.getText().length() > 0 ) {
            float temp = Float.parseFloat(flowText.getText().toString());
            mFlow = Float.parseFloat(String.format("%.1f", temp));
        } else {
            mFlow = 0.0f;
        }

        Context context = SettingsActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.ip_key), mIpAddress);
        editor.putString(getString(R.string.sms_key), mSMSWords);
        editor.putInt(getString(R.string.interval_key), mIntervalValue);
        editor.putInt(getString(R.string.alarm_key), mAlarmValue);
        editor.putFloat(getString(R.string.veryhigh_key), mVeryHigh);
        editor.putFloat(getString(R.string.high_key), mHigh);
        editor.putFloat(getString(R.string.low_key), mLow);
        editor.putFloat(getString(R.string.verylow_key), mVeryLow);
        editor.putFloat(getString(R.string.flow_key), mFlow);
        editor.commit();
        Log.d(TAG, "bindData Complete");
    }

    private void setDisplayData() {
        EditText ipTextBox = (EditText) findViewById(R.id.ipTextBox);
        ipTextBox.setText(mIpAddress);
        EditText smsTextBox = (EditText) findViewById(R.id.smsTextBox);
        smsTextBox.setText(mSMSWords);
        mIntervalSpinner.setSelection(mIntervalValue -1);
//        mAlarmSpinner.setSelection(mAlarmValue);
        EditText alarmTextBox = (EditText) findViewById(R.id.alarmTextBox);
        alarmTextBox.setText("" + mAlarmValue);

        EditText veryHighText = (EditText) findViewById(R.id.veryHighTextBox);
        veryHighText.setText("" + mVeryHigh);

        EditText highText = (EditText) findViewById(R.id.highTextBox);
        highText.setText("" + mHigh);

        EditText lowText = (EditText) findViewById(R.id.lowTextBox);
        lowText.setText("" + mLow);

        EditText veryLowText = (EditText) findViewById(R.id.veryLowTextBox);
        veryLowText.setText("" + mVeryLow);

        EditText flowText = (EditText) findViewById(R.id.flowSettingTextBox);
        flowText.setText("" + mFlow);
    }

    private void getPreferenceData() {

        Context context = SettingsActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);

        mIpAddress = sharedPref.getString(getString(R.string.ip_key), "http://jotarokaga.ddns.net:3000/getCurrInfo");
        mSMSWords = sharedPref.getString(getString(R.string.sms_key), "ข้อความ");
        mIntervalValue = sharedPref.getInt(getString(R.string.interval_key), 1);
        mAlarmValue =  sharedPref.getInt(getString(R.string.alarm_key), 1);
        mVeryHigh = sharedPref.getFloat(getString(R.string.veryhigh_key), 0.0f);
        mHigh = sharedPref.getFloat(getString(R.string.high_key), 0.0f);
        mLow = sharedPref.getFloat(getString(R.string.low_key), 0.0f);
        mVeryLow = sharedPref.getFloat(getString(R.string.verylow_key), 0.0f);
        mFlow = sharedPref.getFloat(getString(R.string.flow_key), 0.0f);

    }

}
