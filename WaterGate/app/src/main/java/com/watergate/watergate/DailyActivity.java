package com.watergate.watergate;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Bryanz on 7/13/2017 AD.
 */

public class DailyActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String TAG = "DailyActivity";
    public static WaterGateProtocol protocol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_activity);
        getSupportActionBar().setTitle(getResources().getString(R.string.action_daily));
        setDate(Calendar.getInstance());
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        setDate(cal);
    }

    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        ((TextView) findViewById(R.id.dateTextView)).setText(dateFormat.format(calendar.getTime()));

        Context context = DailyActivity.this;
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), context.MODE_PRIVATE);
        String ip = sharedPref.getString(getString(R.string.ip_key), "http://jotarokaga.ddns.net:3000");

        String api = "/getHourStat?date=" + calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DATE);
        Log.d(TAG, "Call API:" + ip + api);
        protocol = new WaterGateProtocol(new WaterGateProtocol.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                if (!output.isEmpty()) {
//                    Log.d(TAG, "output:" + output);
                    updateReportDisplay(output);
                }
            }
        }, this, ip, api);
        protocol.execute();
    }

    public void datePicker(View view){

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "date");
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }

    }

    private void updateReportDisplay(String jsonString) {
//        ((TextView) findViewById(R.id.result_test)).setText(jsonString);
        JSONArray jsonArray = new JSONArray();
        String[] header_daiy = {
                getString(R.string.header_time),
                getString(R.string.header_top),
                getString(R.string.header_bottom),
                getString(R.string.header_volume),
                getString(R.string.header_flow),
                getString(R.string.header_cumulative)
        };
        ArrayList<String> data_daily = new ArrayList<>();

        try {
            jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.has("Hour")) {
                    String dateString = jsonObject.getString("Hour");
                    String hardFix = dateString.split("T")[1].split(":")[0];
                    data_daily.add(hardFix+":00");
                } else {
                    data_daily.add("error");
                }
                if (jsonObject.has("headAvg")) {
                    String headAvg = jsonObject.getString("headAvg") == "null" ? "No data" : String.format("%.3f", Float.parseFloat(jsonObject.getString("headAvg")));
                    data_daily.add(headAvg);
                } else {
                    data_daily.add("error");
                }
                if (jsonObject.has("tailAvg")) {
                    String tailAvg = jsonObject.getString("tailAvg") == "null" ? "No data" : String.format("%.3f", Float.parseFloat(jsonObject.getString("tailAvg")));
                    data_daily.add(tailAvg);
                } else {
                    data_daily.add("error");
                }
                if (jsonObject.has("quantityAvg")) {
                    String quantityAvg = jsonObject.getString("quantityAvg") == "null" ? "No data" : String.format("%.3f", Float.parseFloat(jsonObject.getString("quantityAvg")));
                    data_daily.add(quantityAvg);
                } else {
                    data_daily.add("error");
                }
                if (jsonObject.has("flowAvg")) {
                    String flowAvg = jsonObject.getString("flowAvg") == "null" ? "No data" : String.format("%.3f", Float.parseFloat(jsonObject.getString("flowAvg")));
                    data_daily.add(flowAvg);
                } else {
                    data_daily.add("error");
                }
                if (jsonObject.has("cumulative")) {
                    String cumulative = jsonObject.getString("cumulative") == "null" ? "No data" : String.format("%.3f", Float.parseFloat(jsonObject.getString("cumulative")));
                    data_daily.add(cumulative);
                } else {
                    data_daily.add("error");
                }
            }

        } catch (JSONException ex) {
            Toast.makeText(DailyActivity.this, "JSON Data Error", Toast.LENGTH_SHORT);
        }

        GridView headerGrid = (GridView) findViewById(R.id.daily_grid_header);
        ArrayAdapter<String> headerAdapter = new ArrayAdapter<>(this, R.layout.data_grid_list_item_header, header_daiy);
        headerGrid.setAdapter(headerAdapter);

        GridView dailyDataGrid = (GridView) findViewById(R.id.daily_grid);
        ArrayAdapter<String> dailyAdapter = new ArrayAdapter<String>(this, R.layout.data_grid_list_item, data_daily);
        dailyDataGrid.setAdapter(dailyAdapter);


    }
}
